#!/usr/bin/env python3

from pathlib import Path

import click


@click.command()
@click.argument("path")
@click.version_option(
    "0.1.0", message="ls similar with Python and Click (version 0.1.0)"
)
def cli(path):
    target_dir = Path(path)
    if not target_dir.exists():
        click.echo("The target directory doesn't exist")
        raise SystemExit(1)

    for entry in target_dir.iterdir():
        click.echo(f"{entry.name:{len(entry.name) + 5}}", nl=False)

    click.echo()


if __name__ == "__main__":
    cli()
